#!/bin/bash

set -e

inis=$(find S[0-9]* ER[0-9]* O[0-9]* -name *.ini 2>/dev/null | sort)

for ini in ${inis}; do
    echo -n "Validating $ini... "
    python tools/clf-to-omicron.py $ini -o test.ini 1> test.out
    echo "OK"
done
