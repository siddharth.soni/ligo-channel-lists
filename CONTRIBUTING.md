This page outlines the recommended procedure for contributing to the LIGO detchar channel lists.

The workflow should be as follows

- fork the main repository repository:
  - always develop on a [fork](//doc.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork) of the upstream paper repository,
- OR, update an existing fork:
  - make sure your fork is linked to the upstream repo: `git remote add upstream https://git.ligo.org/detchar/ligo-channel-lists.git`
  - then sync the master branch from the upstream repo to your fork: `git pull --rebase upstream master`
- create a new branch on your fork:
  - it is recommended to [develop on a branch](//doc.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork) made specifically for this change - this leaves the `master` branch of your fork as a landing place for upstream changes from other developments,
- make some commits and `git push` them to your fork on git.ligo.org,
- submit a merge request:
  - once all changes are tested and ready, please push your branch to your fork on git.ligo.org, then create a merge request on https://git.ligo.org/detchar/ligo-channel-lists - this will allow the changes to be reviewed before acceptance, modification request, or rejection,
  - if your merge request implements a request already posted in a ticket, include the text _fixes #X_ in the body of your request, allowing issue _#X_ to be closed automatically when the request is merged,
  - any subsequent commits you push to your development branch will automatically be added to the request
- delete your development branch:
  - if the request is merged, git.ligo.org will allow you to delete your development branch, keeping your fork clean for future work
 
Quick links: 

- here's a [quick reference](//gitref.org/basic/) for the basic git commands (init, clone, status, add, commit, push, pull)