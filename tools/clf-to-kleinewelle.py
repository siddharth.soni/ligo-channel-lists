#!/usr/bin/env python
usage       = "clf-to-klienewelle.py [--options] ligoChannelList.ini"
description = "converts a LIGO Channel List INI file into a KlieneWelle config. Prints the resulting config file to stdout if --output-file is not specified."
author      = "reed.essick@ligo.org"

#-------------------------------------------------

import numpy as np

import re
import os
import sys

from collections import defaultdict

import getpass

import time

from ConfigParser import SafeConfigParser

from optparse import OptionParser

#-------------------------------------------------

### known/permissible values of safety and fidelity flags
known_safety   = 'safe unsafe unsafeabove2kHz unknown'.split()
known_fidelity = 'clean flat glitchy unknown'.split()

default_frqmap = {
        16384 : [(8,128), (32,2048), (1024,4096), (2048,8192)],
         8192 : [(8,128), (32,2048), (1024,4096), (2048,4096)],
         4096 : [(8,128), (32,2048), (1024,2048)],
         2048 : [(8,128), (32,1024)],
         1024 : [(8,128), (32,512)],
          512 : [(8,128), (32,256)],
          256 : [(8,128)],
        }

kwconfigHeader = """\
stride         %(stride)d
basename       KW-%(basename)s-%(frameType)s_TRIGGERS
segname        KW-%(basename)s-%(frameType)s_SEGMENTS
significance   %(signif).1f
threshold      %(threshold).1f
decimateFactor %(decimate).0f"""

#-------------------------------------------------

parser = OptionParser(usage=usage, description=description)

### verbosity options
parser.add_option('-v', '--verbose', default=False, action='store_true',
    help='print information' )

parser.add_option('-V', '--Verbose', default=False, action='store_true',
    help='print even more information' )

parser.add_option('-o', '--output-dir', default='.', type='string',
    help='Default="."' )

### error handling
parser.add_option('-f', '--force', default=False, action='store_true',
    help='skip channels if we cannot parse them rather than raising an exception' )

### options to exclude sections
parser.add_option('', '--section-exclude', default=[], type='string', action='append',
    help='exclude these sections of the INI file from the final omegascan config. We require an exact match to exclude a section.' )

### options to exclude chanels
parser.add_option('', '--safety-exclude', default=[], type='string', action='append',
    help='exclude any channel with this safety value. Can supply multiple values by repeating this argument. \
Each must be one of %s'%(", ".join(known_safety)) )

parser.add_option('', '--fidelity-exclude', default=[], type='string', action='append',
    help='exclude any channel with this fidelity value. Can supply multiple values by repeating this argument. \
Each must be on of %s'%(", ".join(known_fidelity)) )

parser.add_option('', '--channel-exclude', default=[], action='append', type='string',
    help='exclude this channel (requires exact match). Can be repeated' )

parser.add_option('', '--regex-channel-exclude', default=[], action='append', type='string',
    help='exclude all channels that match this regular expression. Can be repeated' )

### options to override things in default_frqmap
parser.add_option('', '--freq-map', default=[], nargs=3, action='append', type='int',
    help='overwrite the default frequency mapping from sampling frequency to analysis bandwidths \
(eg: "--freq-map 1024 32 512" will add an analysis band between 32-512 Hz for channels with sampling \
frequencies of 1024 Hz). This argument can be repeated, but if supplied even once will overwrite \
the entire default frequency map. NOTE: if a channel\'s sampling frequency is not present in the \
frequency map, it is not included in the resulting config.' )

### options to override things in the INI file
parser.add_option('', '--f-low', default=None, type='float',
    help='the low frequency cuttoff used in search. DEFAULT=None and this value will be based only on the default frequency map.\
If supplied, this will be applied to all channels independent of the sampling frequencies. Will also overwrite anything specified \
with --freq-map' )

parser.add_option('', '--f-high', default=None, type='float',
    help='the maximum allowed frequency used in the search. DEFAULT=None and this value will be based only on the default frequency map.\
If supplied, this will be applied to all channels independent of the sampling frequencies. Will also overwrite anything specified \
with --freq-map' )

### options specific to KlieneWelle configs
parser.add_option('', '--basename', default=None, type='string',
    help='the basename for the resulting KW triggers. Default=None and will be based of the INI filename' )

parser.add_option('', '--stride', default=None, type='int', 
    help='the stride for the KW analysis. Default=None and will be based on the lowest frequency analyzed.\
If supplied, should be a power of 2' )

parser.add_option('', '--signif', default=15.0, type='float',
    help='the significance threshold for trigger production. Default=15.0' )

parser.add_option('', '--threshold', default=3.0, type='float',
    help='the \"threshold\" parameter in the config\'s header. Default=3.0' )

parser.add_option('', '--decimate', default=-1, type='int',
    help='the \"decimateFactor" parameter in the config\'s header. Default=-1' )

opts, args = parser.parse_args()

#------------------------

### parse out chanlist config name
if len(args)!=1:
    raise ValueError('please supply exactly 1 input argument\n%s'%usage)
chanlist = args[0]

### verbosity
opts.verbose = opts.verbose or opts.Verbose

### ensure --safety-exclude and --fidelity-exclude are permissible values
opts.safety_exclude = sorted(set(opts.safety_exclude)) ### ensure list is unique and sorted
for safety in opts.safety_exclude:
    assert safety   in known_safety,   '--safety-exclude=%s is not understood. Must be one of %s'%(safety, ", ".join(known_safety))

opts.fidelity_exclude = sorted(set(opts.fidelity_exclude)) ### ensure list is unique and sorted
for fidelity in opts.fidelity_exclude:
    assert fidelity in known_fidelity, '--fidelity-exclude=%s is not understood. Must be one of %s'%(fidelity, ", ".join(known_fidelity))

### set up regex channel exclusion
opts.regex_channel_exclude = [re.compile(_) for _ in opts.regex_channel_exclude]

#-------------------------------------------------

### set up frequency map
if opts.freq_map:
    frqmap = defaultdict( list )
    for fs, fm, fM in opts.freq_map:
        frqmap[fs].append( [fm, fM] )

else:
    frqmap = default_frqmap

if opts.f_low!=None: ### supplied, so we need to update the frequency mapping
    if opts.verbose:
        print( 'updating frequency map to only include analysis frequencies above %.3f Hz'%opts.f_low )
    frqmap = dict( (key, [ [max(opts.f_low, fm), fM] for fm, fM in val]) for key, val in frqmap.items() )

if opts.f_high!=None: ### supplied, so we need to update the frequency mapping
    if opts.verbose:
        print( 'updating frequency map to only include analysis frequencies below %.3f Hz'%opts.f_high )
    frqmap = dict( (key, [ [fm, min(opts.f_high, fM)] for fm, fM in val]) for key, val in frqmap.items() )

### clean up frequency map to make sure it's valid
if opts.Verbose:
    print( 'confirming that frequency map is sane' )
for key in frqmap.keys():
    val = [ [fm, fM] for fm, fM in frqmap.pop(key) if fm < fM ] ### downselect based on basic sanity requirement
    if val: ### if there's still something there, add it back in
        frqmap[key] = val

if not frqmap: ### no keys left!
    raise ValueError( 'no valid frequency maps retained!' )

if opts.Verbose:
    print( 'using frequency map:' )
    for key, val in frqmap.items():
        print( 'sampling frequency : %.1f'%key )
        for fm, fM in val:
            print( '    fmin : %10.1f\tfmax : %10.1f'%(fm, fM) )

#-------------------------------------------------

### set up stride
if opts.stride==None: ### not supplied, need to set based on lowest analysis frequency
    fm = min( [ fm for val in frqmap.values() for fm, fM in val ] )
    ### require 64 "independent trials" for statistics/whitening
    ### ensure stride is actually a power of 2, cast to an integer
    opts.stride = int( 2**np.ceil( np.log(64./fm)/np.log(2) ) )

#-------------------------------------------------

### set up basename
if opts.basename==None:
    opts.basename = os.path.basename( chanlist ).strip('.ini').replace('-', '_') ### assumes standard ligo naming convention

#-------------------------------------------------
### iterate over channels, adding them to the config string
#-------------------------------------------------

### read in chanlist 
if opts.verbose:
    print( 'reading ligoChannelList from : %s'%chanlist )
config = SafeConfigParser()
config.read( chanlist )

#-------------------------------------------------

### generate a separate config file for each frame type
frameTypes = defaultdict(list)
for name in config.sections():
    frameTypes[config.get(name,'frametype')].append( name )

### itereate through the different frame types
for frameType, sections in frameTypes.items():

    if opts.verbose:
        print( 'processing frameType=%s'%frameType )

    kwconfig = kwconfigHeader%{
        'stride'    : opts.stride,
        'basename'  : opts.basename,
        'frameType' : frameType,
        'signif'    : opts.signif,
        'threshold' : opts.threshold,
        'decimate'  : opts.decimate,
    }

    for name in sections:

        ### ensure this section should not be skipped!
        if name in opts.section_exclude:
            if opts.Verbose:
                print( "    skipping section : %s because it is in --section-exclude"%(name) )
            continue

        ### actually process this section
        if opts.verbose:
            print( "    processing section : %s"%name )

        #---------------------------------------------
    
        ### extract the low frequency 
        flow = config.getfloat(name, 'flow')

        ### figure out whether to use Nyquist for each channel or a specific limit
        ### NOTE: if useNyquist, we just use the max frequency allowed by Omega
        fhigh      = config.get(name, 'fhigh')
        useNyquist = fhigh == "Nyquist"
        if not useNyquist:
            fhigh = float(fhigh)

        #---------------------------------------------
        ### set up each channel
        #---------------------------------------------
        for channel in config.get(name, 'channels').strip().split('\n'):

            ### parse out expected format for each channel
            channel = channel.split()
            if len(channel)==2: ### backward compatibility with old format
                channel, fsamp = channel
                fsamp    = float(fsamp)
                safety   = "unknown"
                fidelity = "unknown"

            elif len(channel)==4: ### expected format
                channel, fsamp, safety, fidelity = channel
                fsamp = float(fsamp)

            elif opts.force:
                if opts.Verbose:
                    print( "  WARNING: could not parse channel : %s\n    --> skipping!"%(' '.join(channel)) )

            else:
                raise SyntaxError( 'could not parse channel : %s'%(' '.join(channel)) )

            #-----------------------------------------

            ### check that safety and fidelity are permissible values
            assert safety   in known_safety,   'safety=%s is not understood. Must be one of %s'%(safety, ", ".join(known_safety))
            assert fidelity in known_fidelity, 'fidelity=%s is not understood. Must be one of %s'%(fidelity, ", ".join(known_fidelity))

            if opts.Verbose:
                print( "        channel : %s"%channel )

            ### condition on whether or now we want to exclude this
            if channel in opts.channel_exclude:
                if opts.Verbose:
                    print( "            --> skipping Channel=%s because it was specified with --channel-exclude"%(channel) )
                continue

            for regex in opts.regex_channel_exclude:
                if regex.match( channel ):
                    if opts.Verbose:
                        print( "            --> skipping Channel=%s because it matches --regex-channel-exclude=%s"%(channel, regex.pattern) )
                    continue

            if safety in opts.safety_exclude:
                if opts.Verbose:
                    print( "            --> skipping Channel=%s because safety=%s"%(channel, safety) )
                continue

            if fidelity in opts.fidelity_exclude:
                if opts.Verbose:
                    print( "            --> skipping Channel=%s because fidelity=%s"%(channel, fidelity) )
                continue

            #----------------

            ### figure out maximum frequency
            if useNyquist:
                fhigh = fsamp/2.

            ### format the frequency ranges for this channel
            if frqmap.has_key(fsamp): ### there are frequency ranges corresponding to this sampling rate

                for fm, fM in frqmap[fsamp]: ### iterate through bandwidths
                    if opts.f_low==None: ### not suppplied, use what's in the config file
                        fm = max(flow, fm)

                    if opts.f_high==None: ### not supplied, use what's in the config file
                        fM = min(fhigh, fM)

                    if fm < fM: ### ensure this is still sane
                        kwconfig += "\nchannel %-30s %8d %8d"%(channel, fm, fM)

    #---------------------------------------------
    ### write out the config file
    output_file = os.path.join( opts.output_dir, "KW-%s-%s.cnf"%(opts.basename, frameType) )
    if opts.verbose:
        print( "writing KW config to : %s"%output_file )
    file_obj = open(output_file, 'w')
    file_obj.write( kwconfig ) ### NOTE: this way sorta needlessly uses more memory than necessary because I don't every need the full string at once...
    file_obj.close()
