#
# To update channel safety information:
#
# 1. Find latest Hveto configuration:
#    e.g. from https://ldas-jobs.ligo-la.caltech.edu/~detchar/hveto/day/20170307/latest/about/
#
# 2. Copy the list of 'unsafe-channels' into a text file e.g. L1_unsafe.txt
#
# 3. Run this script with this file and the channel list you wish to update as arguments:
#    python update_safety.py L1_safety.txt L1-O2-standard.ini
#
# At the moment, this script will only change designations from 'safe' to 'unsafe'.
#


import sys, os, re

# input arguments
safetyfile = sys.argv[1]
inputfile = sys.argv[2]

# open files
channels = open(inputfile)
safety = open(safetyfile)
output = open(os.path.splitext(inputfile)[0] + '-safety.ini', 'w')

# loop over lines
for line in channels:

    # does line match format 'channel_name rate safety ...'
    matchResult = re.search(r'^\s*([HL]1:[-A-Z0-9_]*) [0-9]{1,5} safe [a-z]*$', line)

    # if it does
    if matchResult:

        # check safety file
        safety.seek(0)
        for unsafe_channel in safety:

            # if this channel is listed as unsafe
            if matchResult.group(1) in unsafe_channel:

                # mark it as unsafe
                line = line.replace('safe', 'unsafe')

    # write line to output file
    output.write(line)


channels.close()
safety.close()
output.close()
